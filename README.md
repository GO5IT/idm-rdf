[![DOI](https://zenodo.org/badge/407187246.svg)](https://zenodo.org/badge/latestdoi/407187246)

# idm-rdf
## Intavia Data Model for RDF data
IDM/ InTaVia data model (in progress) combines CIDOC DRM Version 6.2.1. (to be replaced with upcoming **Cidoc CRM version 7.1.1** as soon as it is published), *BioCRM* extension, *EDM*/ Europana Data Model (based on EDM OWL, aligned with EDM version 5.2.4, last update: 2013-05-20, skos:definition of EDM is implemented as rdfs:comment), *PROV-O* and *Open Archives Initiatives ORE* (Version 1.0, 2008-10-17)).
First integration of **cultural hertage objects** can be found on branch "cho".
